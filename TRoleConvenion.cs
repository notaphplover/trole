﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;

using System.Collections.Generic;
using System.Linq;

namespace TRoleLib
{
    public class TRoleConvenion<Role, Factory> : IApplicationModelConvention
        where Role : TRole<Role>, new()
        where Factory : TRoleFactory<Role>, new()
    {
        #region CONST

        /// <summary>
        /// Alias of the action models property to store the culture.
        /// </summary>
        internal const string TROLE_PROPERTY = "t_roles_allowed";

        #endregion

        #region ATTRIBUTES

        protected Factory factory;

        #endregion

        public TRoleConvenion(Factory factory)
        {
            this.factory = factory;
        }

        public void Apply(ApplicationModel application)
        {
            foreach (ControllerModel controller in application.Controllers)
                foreach (ActionModel action in controller.Actions) {
                    SortedSet<string> allowedAliases = new SortedSet<string>();
                    foreach (TRoleAttribute attribute in action.Attributes.OfType<TRoleAttribute>()) {
                        foreach (string role in attribute.RoleAliases) {
                            if(this.factory.isRole(role))
                                allowedAliases.Add(role);
                        }
                    }

                    if(allowedAliases.Count > 0)
                        action.Properties[TRoleConvenion<Role, Factory>.TROLE_PROPERTY] = allowedAliases;
                }
        }
    }
}
