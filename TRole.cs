﻿using System;
using System.Collections.Generic;

using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace TRoleLib
{
    public class TRole<T> : IdentityRole, IComparable<T>
        where T : TRole<T>, new()
    {
        public SortedSet<T> Children { get; protected set; }

        public SortedSet<T> Parents { get; protected set; }

        public TRole()
            : base()
        {
            this.Children = new SortedSet<T>();
            this.Parents = new SortedSet<T>();
        }

        public TRole(string roleName)
            : base(roleName)
        {
            this.Children = new SortedSet<T>();
        }

        public void addChildren(IEnumerable<T> children)
        {
            foreach (T role in children)
                this.Children.Add(role);
        }

        public void addParents(IEnumerable<T> parents)
        {
            foreach (T role in parents)
                this.Parents.Add(role);
        }

        public virtual int CompareTo(T other)
        {
            return this.Name.CompareTo(other.Name);
        }
    }
}

