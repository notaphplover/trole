﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace TRoleLib
{
    public static class TRoleExtension
    {
        public static void AddTRole<Role, Factory>(this IServiceCollection services, IConfiguration rolesConfiguration)
            where Role : TRole<Role>, new()
            where Factory : TRoleFactory<Role>, new()
        {
            Factory factory = new Factory();
            factory.addRoles(rolesConfiguration);
            services.AddSingleton<Factory>(factory);

            services.AddMvc(o => {
                o.Conventions.Add(new TRoleConvenion<Role, Factory>(factory));
                o.Filters.Add(new TRoleFilter<Role, Factory>(factory));
            });
        }

        public static void UseTRole<User, Role, Factory, DbContext>(this IApplicationBuilder app)
            where User : IdentityUser
            where Role : TRole<Role>, new()
            where Factory : TRoleFactory<Role>, new()
            where DbContext : IdentityDbContext<User>
        {
            DbContext dbContext = app.ApplicationServices.GetService<DbContext>();
            Factory roleFactory = app.ApplicationServices.GetService<Factory>();
            RoleManager<IdentityRole> roleManager = app.ApplicationServices.GetService<RoleManager<IdentityRole>>();

            dbContext.Database.EnsureCreated();

            foreach (Role role in roleFactory.Roles) {
                var existRole = roleManager.RoleExistsAsync(role.Name);
                existRole.Wait();
                if (!existRole.Result)
                    roleManager.CreateAsync(new IdentityRole(role.Name)).Wait();
            }
        }
    }
}
