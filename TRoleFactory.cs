﻿using TRoleLib;

using Microsoft.Extensions.Configuration;

using System;
using System.Collections.Generic;

namespace TRoleLib
{
    public interface ITRoleFactory
    {
        void addRoles(IConfiguration rolesConfiguration);
    }

    public class TRoleFactory<T> : ITRoleFactory
        where T : TRole<T>, new()
    {
        #region ATTRIBUTES

        protected SortedDictionary<string, T> rolesDictionary;

        #endregion

        #region PROPERTIES

        public ICollection<T> Roles { get { return this.rolesDictionary.Values; } }

        #endregion

        public TRoleFactory()
        {
            this.rolesDictionary = new SortedDictionary<string, T>();
        }

        public TRoleFactory(IConfiguration rolesConfiguration) 
            : this()
        {
            this.addRoles(rolesConfiguration);
        }

        public virtual void addRoles(IConfiguration rolesConfiguration)
        {
            IConfigurationSection rolesSection = rolesConfiguration.GetSection("TRoles");

            SortedDictionary<string, LinkedList<string>> inheritedRoles = new SortedDictionary<string, LinkedList<string>>();

            //Step 1: get role aliases and direct children
            foreach (IConfigurationSection roleConfig in rolesSection.GetChildren()) {
                string roleAlias = roleConfig.Key;

                this.rolesDictionary.Add(roleAlias, new T { Name = roleAlias });

                LinkedList<string> inheritRolesAliases = new LinkedList<string>();

                foreach (IConfigurationSection inheritRole in roleConfig.GetSection("inherits").GetChildren()) {
                    inheritRolesAliases.AddLast(inheritRole.Value);
                }

                inheritedRoles.Add(roleAlias, inheritRolesAliases);
            }

            //Step 2. Go deep for not direct inheritances.
            foreach (KeyValuePair<string, LinkedList<string>> roleData in inheritedRoles) {
                SortedDictionary<string, T> processedRoles = new SortedDictionary<string, T>();
                LinkedList<string> notProcessedRoles = new LinkedList<string>();

                T rolePivot;

                if (!this.rolesDictionary.TryGetValue(roleData.Key, out rolePivot))
                    throw new Exception("It was found the undefined role: " + roleData.Key + " in a json configuration file.");

                foreach (string roleS in roleData.Value)
                    notProcessedRoles.AddLast(roleS);

                while (notProcessedRoles.Count > 0) {
                    string first = notProcessedRoles.First.Value;

                    if (!processedRoles.ContainsKey(first)) {
                        T role;

                        if (!this.rolesDictionary.TryGetValue(first, out role))
                            throw new Exception("It was found the undefined role: " + first + " in a json configuration file.");

                        processedRoles.Add(first, role);

                        foreach (string roleS in inheritedRoles[first])
                            if(!processedRoles.ContainsKey(roleS))
                                notProcessedRoles.AddLast(roleS);
                    }

                    notProcessedRoles.RemoveFirst();
                }

                foreach (T inheritedRole in processedRoles.Values) {
                    rolePivot.Children.Add(inheritedRole);
                    inheritedRole.Parents.Add(rolePivot);
                }
                    
            }
        }

        public virtual T createRole(string roleName) {
            T prototype;

            if (!this.rolesDictionary.TryGetValue(roleName, out prototype))
                throw new ArgumentException("There is no role with the name \"" + roleName + "\".");

            T newRole = new T { Name = roleName };
            newRole.addChildren(prototype.Children);
            newRole.addParents(prototype.Parents);
            return newRole;
        }

        public virtual bool isRole(string roleName) {
            return this.rolesDictionary.ContainsKey(roleName);
        }
    }
}
