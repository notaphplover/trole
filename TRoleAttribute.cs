﻿using System;
using System.Collections.Generic;

namespace TRoleLib
{
    public class TRoleAttribute : Attribute
    {
        #region ATTRIBUTES

        protected SortedSet<string> allowedRolesAliases;

        public TRoleAttribute(params string[] roleAliases)
        {
            this.allowedRolesAliases = new SortedSet<string>();
            foreach (string alias in roleAliases)
                this.allowedRolesAliases.Add(alias);
        }

        public ICollection<string> RoleAliases { get { return this.allowedRolesAliases; } }

        #endregion
    }
}
