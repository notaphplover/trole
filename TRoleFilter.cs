﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

using System;
using System.Collections.Generic;
using System.Net;

namespace TRoleLib
{
    public class TRoleFilter<Role, Factory> : IAuthorizationFilter
        where Role : TRole<Role>, new()
        where Factory : TRoleFactory<Role>, new()
    {
        #region ATTRIBUTES

        protected Factory factory;

        #endregion

        public TRoleFilter(Factory factory)
        {
            this.factory = factory;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            object allowedAliasesObj;

            if (context.ActionDescriptor.Properties.TryGetValue(TRoleConvenion<Role, Factory>.TROLE_PROPERTY, out allowedAliasesObj) && allowedAliasesObj is SortedSet<string>) {
                SortedSet<string> allowedAliases = allowedAliasesObj as SortedSet<string>;

                bool allow = false;

                foreach (string role in allowedAliases)
                    if (context.HttpContext.User.IsInRole(role)) {
                        allow = true;
                        break;
                    } else {
                        if (this.factory.isRole(role)) {
                            ICollection<Role> roleParents = this.factory.createRole(role).Parents;

                            foreach (Role roleParent in roleParents) {
                                if (context.HttpContext.User.IsInRole(roleParent.Name)) {
                                    allow = true;
                                    break;
                                }
                            }
                        }
                    }

                if (!allow)
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            }
        }
    }
}
